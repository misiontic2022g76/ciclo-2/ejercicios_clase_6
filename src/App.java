public class App {
    public static void main(String[] args) throws Exception {
        
        //Construir un avión de carga
        System.out.println("------AVION DE CARGA------");
        AvionCarga objAvionCarga = new AvionCarga("Gris",150.5);
        objAvionCarga.cargar();        
        System.out.println(objAvionCarga.despegar());
        objAvionCarga.aterrizar();
        objAvionCarga.descargar();
        

        //Cosntruir avión de pasajeros
        System.out.println("------AVION DE PASAJEROS------");
        AvionPasajeros objAvionPasajeros = new AvionPasajeros("Gris", 112.5, 6);
        objAvionPasajeros.despegar();
        objAvionPasajeros.servir();
        objAvionPasajeros.aterrizar();
        objAvionPasajeros.frenar();
        

        System.out.println("--------AVION MILITAR--------");
        AvionMilitar objAvionMilitar = new AvionMilitar("Verde", 181.12,500);
        objAvionMilitar.setMisiles(6); //Le digo que el avion militar tendrá 6 misiles
        objAvionMilitar.despegar();
        objAvionMilitar.detectar_amenaza(false); //no detecta amenaza
        for (int i = 0; i < 8; i++) {
            objAvionMilitar.detectar_amenaza(true);//es una amenaza. Le mando varias amenazas y se queda sin misiles     
        }        
        objAvionMilitar.aterrizar();
        objAvionMilitar.frenar();

        System.out.println("---- AVION MILITAR - INGENIERO ----");
        
        //Crear objeto tipo ingeniero
        Ingeniero objIngeniero = new Ingeniero();
        //Construye avión militar
        objIngeniero.construir_avion("rojo", 10, 12);
        AvionMilitar objAvionMilitar = objIngeniero.getAvionMilitar(0);
        System.out.println("-Objeto Constrido-");
        System.out.println(objAvionMilitar); //no es legible, sale un hexagesimal pero se valida que hay objeto
        System.out.println("-Acceder al objeto avion-");
        objAvionMilitar.despegar(); 
        //La linea anterior almacena en una variable de tipo Avion militar,
        //llamada objAvionMilitar, lo que se encuentra en la posición 0 
        //de la lista.
       
        
    }
}
