public class AvionPasajeros extends Avion{

    /**********
     * Atributos
     *************/
    private int pasajeros;


    /*******************
     * Constructor para enviar informacion a Avion
     *******************/
    public AvionPasajeros(String color, double tamano, int pasajeros){
             super(color, tamano);
             this.pasajeros=pasajeros; //asigno al atributo pasajeros lo que tomo de pasajeros
    }


    /*************
     * Métodos
     **********/
    public void servir(){
        //System.out.println("Sirviendo a pasajeros: "+pasajeros);
        
         for (int i = 1; i <= pasajeros; i++) {
            System.out.println("Sirviendo al pasajero "+i);
        }
        
        
    }



}
