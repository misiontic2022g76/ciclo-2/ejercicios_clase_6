public class AvionMilitar extends Avion{

    /***********
     * Atributos
     ***********/
    private int misiles;

    /**************************************
     * CONSTRUCTOR PARA CONECTAR CON AVION
     **************************************/
    public AvionMilitar(String color, double tamano, int misiles){
        super(color, tamano);
    }
    public AvionMilitar(String color, double tamano){
        super(color, tamano);
    }
 
     //Modificador
     public void setMisiles(int misiles){
         this.misiles = misiles;
     }

    /**************
     * Métodos
     *************/

     //Detectar objetivo
     public void detectar_amenaza(boolean amenaza){
         if (amenaza) {
             this.disparar();
         }else{
             System.out.println("No es una amenaza");
         }
     }

    private void disparar(){
        if(this.misiles>0){
            System.out.println("Disparando...");
            -- this.misiles; //disminuye en 1 el numero de misiles cada que dispara
           
        } else{
            System.out.println("No hay misiles");
        }
 
    }
}