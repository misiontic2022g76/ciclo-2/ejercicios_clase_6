import java.util.ArrayList;
import java.util.List;

public class Ingeniero {

    /************
     * Atributos
     ************/
    private String nombre;
    private String apellido;
    private String cedula;
    private List <AvionMilitar> aviones; //dentro del <> le digo qué me va a almacenar
// guardará AvionMilitar en un atributo llamado aviones

    /************************
     * Método constructor - Si no lo escribo, es vacío
     * 
     ************************/
    public Ingeniero(){
        this.aviones=new ArrayList<AvionMilitar>();
         
    }

    /************
     * Consultor
     * **********   
     */
    public AvionMilitar getAvionMilitar(int posicion){
        return this.aviones.get(posicion);
    }

    
     /********************
      * Métodos
      *******************/
    public boolean construir_avion(String color, double tamano, int misiles){
        AvionMilitar objAvionMilitar = new AvionMilitar(color, tamano, misiles);    //Decirle que construya un avion
        this.aviones.add(objAvionMilitar); //cuando este ingeniero cree un avion, se guardará en aviones.
        return true;
    }
}
