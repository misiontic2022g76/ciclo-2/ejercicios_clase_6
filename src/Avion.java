/**
 * Autor:
 * Empresa:
 * Fecha:
 * Ciudad:
 * Descripción del proyecto:
 */

public class Avion {
    

    /***********
     * Atributos
     * **************/
    private String color;
    private double tamano;



    /***********************
     * Método Constructor
     ***********************/
    //Metodo constructor 1: Requiere obligatoriamente color y tamaño para construir un avion
    public Avion(String color, double tamano){

}
    //Método constructor 2: No requiere obligatoriamente esos dos parámetros para construir un objeto
    /**public Avion(){

    }
    */

/***************
 * Métodos
 * Acciones de la clase
 * *************/
public void aterrizar(){
    System.out.println("Aterrizando...");
}

public boolean despegar(){
    System.out.println("Despegando...");
    return true;
}

public void frenar(){
    System.out.println("Frenar...");
}

}

